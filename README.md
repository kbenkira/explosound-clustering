ExplosoundCluster
=================

Files related to the Startup Weekend competition that took place in Compiègne in 2014, and specifically to the Python side of the project (clustering music samples based on signal features)